Every page in drupal can have a help message displayed to guide visitors 
to your site in to the proper usage of various features.  A clean install 
of Drupal does not provide any way of editing these help messages in a visual 
way.  If you want to add any help messages to your site you would need to 
write your own module, which many people do not know, or even want to know, 
how to do.

This module provides the ability to assign help messages to any page on a 
drupal site through a simple interface in the administration section of the
site.

Send comments to aran@electroniclife.org

Note: Tables still need to be written and SQL needs to be 
tested on other databases.  Until that happens helpedit only 
support MySQL.

Installation
--------
- Copy helpedit.module to your modules directory.
- Run the SQL in helpedit.mysql.
- Go to administer->modules and enable the helpedit module.
- Go to administer->users->configure->permissions and modify 
  the "customize help" permission to suite your needs.

Usage
--------
- Go to administer->help->custom to get started.

